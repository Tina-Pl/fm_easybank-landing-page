import React from 'react';
import classes from './NavbarToggle.module.scss';
import NavbarOpen from '../../../assets/images/icon-hamburger.svg';
import NavbarClose from '../../../assets/images/icon-close.svg';

const NavbarToggle = props => {
  return (
    <div className={classes.NavbarToggle} onClick={props.clicked}>
      {props.showToggler ? (
        <img src={NavbarOpen} alt="Navbar Toggler" />
      ) : (
        <img src={NavbarClose} alt="Navbar Toggler" />
      )}
    </div>
  );
};

export default NavbarToggle;
