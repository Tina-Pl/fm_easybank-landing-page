import React from 'react';
import classes from './NavbarMenuItem.module.scss';

const NavbarMenuItem = props => (
  <li className={classes.NavbarMenuItem}>
    <a className={classes.NavbarMenuItemLink} href={props.scrollTo}>
      {props.children}
    </a>
  </li>
);

export default NavbarMenuItem;
