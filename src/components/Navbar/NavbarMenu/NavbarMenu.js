import React from 'react';
import classes from './NavbarMenu.module.scss';
import NavbarMenuItem from './NavbarMenuItem/NavbarMenuItem';

const NavbarMenu = props => (
  <ul className={classes.NavbarMenu}>
    <NavbarMenuItem scrollTo="#home">Home</NavbarMenuItem>
    <NavbarMenuItem scrollTo="#about">About</NavbarMenuItem>
    <NavbarMenuItem scrollTo="#contact">Contact</NavbarMenuItem>
    <NavbarMenuItem scrollTo="#articles">Blog</NavbarMenuItem>
    <NavbarMenuItem scrollTo="#home">Careers</NavbarMenuItem>
  </ul>
);

export default NavbarMenu;
