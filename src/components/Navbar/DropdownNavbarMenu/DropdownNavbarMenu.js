import React from 'react';
import NavbarMenu from '../NavbarMenu/NavbarMenu';
import classes from './DropdownNavbarMenu.module.scss';

const DropdownNavbarMenu = props => {
  let addedClasses = [classes.Dropdown, classes.Closed];
  if (props.open) {
    addedClasses = [classes.Dropdown, classes.Open];
  }
  return (
    <div className={addedClasses.join(' ')} onClick={props.closed}>
      <nav className={classes.DropdownMenu}>
        <NavbarMenu />
      </nav>
    </div>
  );
};

export default DropdownNavbarMenu;
