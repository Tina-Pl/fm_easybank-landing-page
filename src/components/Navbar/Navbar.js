import React from 'react';
import classes from './Navbar.module.scss';
import { ReactComponent as Logo } from '../../assets/images/logo.svg';
import NavbarMenu from './NavbarMenu/NavbarMenu';
import NavbarToggle from './NavbarToggle/NavbarToggle';
import Button from '../UI/Button/Button';

const Navbar = props => (
  <div>
    <nav className={classes.Navbar}>
      <Logo />
      <div className={classes.DesktopOnly}>
        <NavbarMenu />
      </div>

      <Button btnType="NavbarBtn">Request Invite</Button>
      <NavbarToggle
        clicked={props.navbarToggleClicked}
        showToggler={props.open}
      />
    </nav>
  </div>
);

export default Navbar;
