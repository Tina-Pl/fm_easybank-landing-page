import React from 'react';
import classes from './Articles.module.scss';
import ArticleItems from './ArticleItems/ArticleItems';

const Articles = () => (
  <div className={classes.Articles} id="articles">
    <h1 className={classes.ArticlesTitle}>Latest Articles</h1>
    <ArticleItems />
  </div>
);

export default Articles;
