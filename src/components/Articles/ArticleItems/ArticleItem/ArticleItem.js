import React from 'react';
import classes from './ArticleItem.module.scss';

const ArticleItem = props => (
  <li className={classes.ArticleItem}>
    <img className={classes.ArticleImg} src={props.image} alt="Article image" />
    <div className={classes.ArticleBody}>
      <span className={classes.ArticleAuthor}>By {props.author}</span>
      <h3 className={classes.ArticleTitle}>{props.title}</h3>
      <p className={classes.ArticleText}>{props.text}</p>
    </div>
  </li>
);

export default ArticleItem;
