import React from 'react';
import classes from './ArticleItems.module.scss';
import ArticleItem from './ArticleItem/ArticleItem';
import CurrencyImg from '../../../assets/images/image-currency.jpg';
import RestaurantImg from '../../../assets/images/image-restaurant.jpg';
import PlaneImg from '../../../assets/images/image-plane.jpg';
import ConfettiImg from '../../../assets/images/image-confetti.jpg';

const ArticleItems = () => (
  <div className={classes.ArticleItemsContainer}>
    <ul className={classes.ArticleItems}>
      <ArticleItem
        image={CurrencyImg}
        author="Claire Robinson"
        title="Recieve money in any currency with no fees"
        text="The world is getting smaller and we're becoming more mobile.
        So why should you be forced to only recieve money in a single..."
      />
      <ArticleItem
        image={RestaurantImg}
        author="Wilson Hutton"
        title="Treat yourself without worrying about money"
        text="Our simple budgeting feature allows you to separate out your
        spending and set realistic limits each month. That means you..."
      />
      <ArticleItem
        image={PlaneImg}
        author="Wilson Hutton"
        title="Take your Easybank card wherever you go"
        text="We want you to enjoy your travels. This is why we don't charge
        any fees on purchases while you're abroad. We'll even show you..."
      />
      <ArticleItem
        image={ConfettiImg}
        author="Claire Robinson"
        title="Our invite-only Beta accounts are now live!"
        text="After a lot of hard work by the whole team, we're excited to launch
        our closed beta. It's easy to request an invite through the site..."
      />
    </ul>
  </div>
);

export default ArticleItems;
