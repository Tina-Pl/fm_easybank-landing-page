import React from 'react';
import classes from './CardItem.module.scss';

const CardItem = props => (
  <li className={classes.CardItem}>
    <img src={props.icon} alt="Card" />
    <h2>{props.title}</h2>
    <p>{props.text}</p>
  </li>
);

export default CardItem;
