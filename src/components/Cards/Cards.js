import React from 'react';
import classes from './Cards.module.scss';
import CardItem from './CardItem/CardItem';
import OnlineIcon from '../../assets/images/icon-online.svg';
import BudgetingIcon from '../../assets/images/icon-budgeting.svg';
import OnboardingIcon from '../../assets/images/icon-onboarding.svg';
import APIIcon from '../../assets/images/icon-api.svg';

const Cards = () => (
  <div className={classes.CardsContainer}>
    <ul className={classes.Cards}>
      <CardItem
        icon={OnlineIcon}
        title="Online Banking"
        text="Our modern web and mobile applications allow you to 
        keep track of your finances wherever you are in the world."
      />

      <CardItem
        icon={BudgetingIcon}
        title="Simple Budgeting"
        text="See exactly where your money goes each month. Recieve 
        notification when you're close to hitting your limits."
      />
      <CardItem
        icon={OnboardingIcon}
        title="Fast Onboarding"
        text="We don't do branches. Open your account in minutes online 
        and start taking control of your finances right away."
      />
      <CardItem
        icon={APIIcon}
        title="Open API"
        text="Manage your your savings, investments, pension, and much more
        from one account. Tracking your money has never been easier."
      />
    </ul>
  </div>
);

export default Cards;
