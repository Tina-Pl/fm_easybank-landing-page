import React from 'react';
import classes from './About.module.scss';
import Cards from '../Cards/Cards';

const About = () => (
  <div className={classes.About} id="about">
    <h1 className={classes.AboutTitle}>Why choose Easybank?</h1>
    <p className={classes.AboutText}>
      We leverage Open Banking to turn your bank account into your financial
      hub. Control your finances like never before.
    </p>
    <Cards />
  </div>
);

export default About;
