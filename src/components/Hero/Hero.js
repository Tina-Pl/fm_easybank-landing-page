import React from 'react';
import classes from './Hero.module.scss';
import Button from '../UI/Button/Button';
//import heroImageBg from '../../assets/images/bg-intro-desktop.svg';
import heroImage from '../../assets/images/image-mockups.png';

const Hero = () => (
  <div className={classes.HeroContainer}>
    <div className={classes.Hero}>
      <div className={classes.HeroInfo}>
        <h2 className={classes.HeroTitle}>Next generation digital banking</h2>
        <p className={classes.HeroText}>
          Take your financial life online. Your Easybank account will be
          one-stop-shop for spending, saving, budgeting, investing, and much
          more.
        </p>
        <Button btnType="PageBtn">Request Invite</Button>
      </div>
      <div className={classes.HeroBg}>
        <img
          className={classes.HeroImage}
          src={heroImage}
          alt="Hero Image Background"
        />
      </div>
    </div>
  </div>
);

export default Hero;
