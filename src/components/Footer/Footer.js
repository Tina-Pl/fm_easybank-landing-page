import React from 'react';
import classes from './Footer.module.scss';
import Button from '../UI/Button/Button';
import { ReactComponent as FacebookIcon } from '../../assets/images/icon-facebook.svg';
import { ReactComponent as YoutubeIcon } from '../../assets/images/icon-youtube.svg';
import { ReactComponent as TwitterIcon } from '../../assets/images/icon-twitter.svg';
import { ReactComponent as PinterestIcon } from '../../assets/images/icon-pinterest.svg';
import { ReactComponent as InstagramIcon } from '../../assets/images/icon-instagram.svg';
import { ReactComponent as EasybankLogo } from '../../assets/images/logo.svg';

const Footer = () => (
  <div className={classes.Footer} id="contact">
    <div className={classes.FooterLeft}>
      <div className={classes.SocialMedia}>
        <a href="#home">
          <EasybankLogo className={classes.Logo} />
        </a>
        <div className={classes.SocialLinks}>
          <a
            className={classes.SocialLink}
            href="http://www.facebook.com"
            target="_blank"
          >
            <FacebookIcon />
          </a>
          <a
            className={classes.SocialLink}
            href="http://youtube.com"
            target="_blank"
          >
            <YoutubeIcon />
          </a>
          <a
            className={classes.SocialLink}
            href="http://twitter.com"
            target="_blank"
          >
            <TwitterIcon />
          </a>
          <a
            className={classes.SocialLink}
            href="http://pinterest.com"
            target="_blank"
          >
            <PinterestIcon />
          </a>
          <a
            className={classes.SocialLink}
            href="http://instagram.com"
            target="_blank"
          >
            <InstagramIcon />
          </a>
        </div>
      </div>
      <div className={classes.MenuNav}>
        <ul className={classes.MenuNavLinks}>
          <li className={classes.MenuNavItem}>
            <a className={classes.MenuNavItemLink} href="#about">
              About Us
            </a>
          </li>
          <li className={classes.MenuNavItem}>
            <a className={classes.MenuNavItemLink} href="#contact">
              Contact
            </a>
          </li>
          <li className={classes.MenuNavItem}>
            <a className={classes.MenuNavItemLink} href="#articles">
              Blog
            </a>
          </li>
        </ul>
        <ul className={classes.MenuNavLinks}>
          <li className={classes.MenuNavItem}>
            <a className={classes.MenuNavItemLink} href="#home">
              Careers
            </a>
          </li>
          <li className={classes.MenuNavItem}>
            <a className={classes.MenuNavItemLink} href="#home">
              Support
            </a>
          </li>
          <li className={classes.MenuNavItem}>
            <a className={classes.MenuNavItemLink} href="#home">
              Privacy Policy
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div className={classes.FooterRight}>
      <Button btnType="PageBtn">Request Invite</Button>
    </div>
  </div>
);

export default Footer;
