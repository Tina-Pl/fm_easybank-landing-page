import './App.scss';
import React, { Component } from 'react';
import Navbar from './components/Navbar/Navbar';
import DropdownNavbar from './components/Navbar/DropdownNavbarMenu/DropdownNavbarMenu';
import Hero from './components/Hero/Hero';
import About from './components/About/About';
import Articles from './components/Articles/Articles';
import Footer from './components/Footer/Footer';

class App extends Component {
  state = {
    showDropdown: false,
    showHamburger: true,
  };

  dropdownToggleHandler = () => {
    this.setState(prevState => {
      return {
        showDropdown: !prevState.showDropdown,
        showHamburger: !prevState.showHamburger,
      };
    });
  };

  dropdownCloseHandler = () => {
    this.setState({ showDropdown: false, showHamburger: true });
  };

  render() {
    return (
      <div className="App" id="home">
        <Navbar
          navbarToggleClicked={this.dropdownToggleHandler}
          open={this.state.showHamburger}
        />
        <DropdownNavbar
          open={this.state.showDropdown}
          closed={this.dropdownCloseHandler}
        />
        <Hero />
        <About />
        <Articles />
        <Footer />
      </div>
    );
  }
}

export default App;
